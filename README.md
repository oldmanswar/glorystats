# GloryStats or Glorified Statistics
##### (also known as Artificial Intelligence)

## Description
A library to simplify and automate certain functions useful in machine learning projects.  

### Naive Bayes
Performs a Naive Bayes matrix sampling of hyper-parameters for machine learning models like Support Vector Machines and Random Forest. It is useful to make predictions on wether a data set is optimal or not for artificial neural network training.  

### Geo-Spatial
Automates the collection of data from diverse geo-spatial sources like NASA SRTM and ArcGIS. It supports multiple data formats (srtm, cdf, Slippy maps) and automatically converts it to Pandas data frames. Great for pipelines as it is pure Python and does not need to be run within a Jupyter notebook.

### GS Keras
Just a TensorFlow Keras wrapper optimised for geo data handling.

## Visuals
Visuals are created (mostly) using the Plotly library.

## Installation
A work in progress, the best way to go right now is to copy paste the contents of the library files into your Jupyter notebook or run the Python files at the start of your notebook file.  

## Usage
As above, until the installation procedure is finished do a copy paste and call the functions in the classes. Take a peek at the demos folder to get a handle of how to use it in your own projects.  

## Support
Send me a message if you have problems, I'll try not to ignore you.

## Roadmap
I'm busy with other stuff right now so I do not even have a roadmap for this. In any case, software projects tend to by unidimensional so there is not much use for a roadmap. The only way is forward.

## Contributing
Right now it's mine and only mine.  
And I'm sure you can do much better on your own.

## Authors and acknowledgment
Just me with some sprinkles from Michaela Dillon.

## License
It's GPL. If you want to use it, ask for permission... and on your own risk, I do not claim it is fit for any particular purpose.

## Project status
A total disaster. A lot like my life.  
LOL, just kidding, it's not that bad...  
