# a complement lib for Keras

import plotly.graph_objs as go

def normalise(df,column_names):
    normal_names=[]
    if type(column_names)==str:
        column_names=[column_names]
    for column_name in column_names:
        maxi=df[column_name].max()
        mini=df[column_name].min()
        delta=maxi-mini
        new_name=column_name+'_n'
        df[new_name]=((df[column_name]-mini)/delta).astype('float32')
        normal_names.append(new_name)
    return normal_names
    
def history_trace(df,column,name=None):
    if name is None:
        name=column
    trace=go.Scatter(y=df[column],name=name,mode='lines')
    return trace

def history_fig(traces,title='training epochs'):
    layout = go.Layout(margin=dict(l=5,
                                   r=5,
                                   b=5,
                                   t=30))

    fig = go.Figure(data=traces, layout=layout)

    fig.update_layout(title=dict(text=title, font=dict(size=20), yref='paper')
                      ,xaxis_title="Epochs",yaxis_title="Error (m)")
    return fig