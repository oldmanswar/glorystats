# dependencies

# imports
import os
import copy
from io import BytesIO
import requests
import math
import numpy as np
import pandas as pd
from PIL import Image
import netCDF4 as cdf
import arcgis
import arcgis.raster.functions as agf
from pyproj import Transformer
import plotly.graph_objs as go

# common functions
def swap_values(a,b):
    return b,a

def mercator_proportion(lon_min: float,lat_min: float,lon_max: float,lat_max: float):
    if lat_max<lat_min:
            (lat_max,lat_min)=swap_values(lat_max,lat_min)
    if lon_max<lon_min:
        (lon_max,lon_min)=swap_values(lon_max,lon_min)
    lat_avg=(lat_max+lat_min)/2
    return (lon_max-lon_min)/(lat_max-lat_min)*math.cos(lat_avg*math.pi/180)

def merc_prop_df(df):
    return mercator_proportion(df['lon'].min(),df['lat'].min(),df['lon'].max(),df['lat'].max())

def data_extension(df):
    return (df['lon'].min().copy()
                    ,df['lat'].min().copy()
                    ,df['lon'].max().copy()
                    ,df['lat'].max().copy())

def search_multispectral():
    items=gis.content.search('"Multispectral Landsat"', 'Imagery Layer')
    for item in items:
        display(item)
        
def normalisers(df,column_name):
    max=df[column_name].max()
    min=df[column_name].min()
    delta=max-min
    return delta,min

def normalise(df,column_names):
    normal_names=[]
    if type(column_names)==str:
        column_names=[column_names]
    for column_name in column_names:
        maxi=df[column_name].max()
        mini=df[column_name].min()
        delta=maxi-mini
        new_name=column_name+'_n'
        df[new_name]=((df[column_name]-mini)/delta).astype('float32')
        normal_names.append(new_name)
    return normal_names
    
gps2wm = Transformer.from_crs("EPSG:4326", "EPSG:3857")
wm2gps = Transformer.from_crs("EPSG:3857","EPSG:4326")

# graphics functions

def create_scale(min_depth,max_depth,zero=0):
    if max_depth<=0:
        colorscale=[[0,'darkblue']
                ,[0.5,'blue']
                ,[0.8,'lightblue']
                ,[1,'white']]
        return colorscale
    if min_depth>0:
        border=1.001*(max_depth-min_depth)/2
    else:
        border=(-min_depth+zero)/(max_depth-min_depth)
    print(min_depth,border,max_depth)
    colorscale=[[0,'blue']
                ,[border*0.9998,'lightblue']
                ,[border*0.9999,'black']
                ,[border*1.0001,'black']
                ,[border*1.001,'beige']
                ,[border*1.1,'darkgreen']
                ,[0.5,'green']
                ,[0.8,'lightgreen']
                ,[1,'white']]
    return colorscale

def scatter_trace(df,altitude,name='altitude',colours=None,scale=None):
    if colours is None:
        if scale is None:
            marker=dict(size=1,opacity=1)
        else:
            marker = dict(
                size = 1,
                color = df[altitude], # set color to an array/list of desired values
                colorscale = scale
                )
    else:
        marker=dict(size=2,
                  color=['rgb({},{},{})'.format(r,g,b) 
                         for r,g,b in zip(df[colours[0]], df[colours[1]], df[colours[2]])],
                  opacity=0.9,)
    trace = go.Scatter3d(x=df.lon
              ,y=df.lat
              ,z=df[altitude]
              ,name=name
              ,mode='markers'
              ,marker=marker)
    return trace

def figure_from_traces(traces: list,title: str,proportion=100.0):
    layout = go.Layout(margin=dict(l=5,
                                   r=5,
                                   b=5,
                                   t=30))

    fig = go.Figure(data=traces, layout=layout)

    camera = dict(eye=dict(x=0.0, y=0.0, z=100)
                          ,up=dict(x=0.0, y=1.0, z=0.0))
    fig.update_layout(scene_aspectmode='manual'
        ,title=dict(text=title, font=dict(size=20), yref='paper')
        ,scene_aspectratio=dict(x=100*proportion
                                ,y=100.0,z=5)
        ,scene=dict(zaxis=dict(nticks=1))
        ,scene_camera=camera
        )
    return fig

# slippy maps
# tiles are referenced from their upper left corner
class Slippy:
    def __init__(self,sm_url: str = "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"):
        self.url=sm_url
    
    def get_zxy(self,zoom: float,lon_deg: float,lat_deg: float):
        z=int(zoom)
        lat_rad=lat_deg/180.0*math.pi
        lon_rad=lon_deg/180.0*math.pi
        n=1 << z
        x=n*((lon_deg+180.0)/360)
        y=n*(1.0-(math.log(math.tan(lat_rad)+1/math.cos(lat_rad))/math.pi))/2
        
        return (z,int(x),int(y))
    
    def get_lon_lat(self,zoom: int,x: int,y: int):
        z = int(zoom)
        n=1 << z
        lon_deg=(x/n)*360.0-180.0
        lat_rad=math.atan(math.sinh(math.pi*(1-2*y/n)))
        lat_deg=lat_rad*180.0/math.pi
        return (lon_deg,lat_deg)
    
    def get_tile(self,z: int,x: int,y: int):
        url=self.url.format(z=z,x=x,y=y)
        response=requests.get(url)
        
        return Image.open(BytesIO(response.content))
    
    def get_image(self,zoom: float,lon: float,lat: float):
        (z,x,y)=self.get_zxy(zoom,lon,lat)
        return self.get_tile(z,x,y)
    
    def set_deltas(self,z,x,y,w,h):
        (lon0,lat0)=self.get_lon_lat(z,x,y)
        (lon1,lat1)=self.get_lon_lat(z,x+1,y+1)
        self.lon_delta=abs(lon1-lon0)/h
        self.lat_delta=abs(lat1-lat0)/w
    
    def get_mosaic(self,zoom: float,lon_min: float,lat_min: float,lon_max: float,lat_max: float):
        if lon_min>lon_max:
            lon_min,lon_max=swap_values(lon_min,lon_max)
        if lat_min>lat_max:
            lat_min,lat_max=swap_values(lat_min,lat_max)
        (z,x_min,y_max)=self.get_zxy(zoom,lon_min,lat_min)
        (z,x_max,y_min)=self.get_zxy(zoom,lon_max,lat_max)
        x_t=abs(x_max-x_min)+1
        y_t=abs(y_max-y_min)
        (self.lon_min,self.lat_min)=self.get_lon_lat(z,x_min,y_max)# tiles are numbered from north to south
        print("min lon lat {},{}".format(self.lon_min,self.lat_min))
        tile = None
        w,h=0,0
        img=None 
        for i in range(x_t):
            for j in range(y_t):
                #print("{},{}".format(i,j))
                #print("{},{}".format(x_min+i,y_min+j))
                #print("{}".format(self.get_lon_lat(z,x_min+i,y_min+j)))
                tile=self.get_tile(z,x_min+i,y_min+j)
                w,h=tile.size
                if img is None:
                    img=Image.new('RGB',(x_t*w,y_t*h))
                img.paste(tile,(i*w,j*h))
        self.set_deltas(z,x_min,y_max,w,h)
        self.img=img
        self.w,self.h=img.size
        return img
    
    def to_xy(self,lon: float,lat: float):
        x=(lon-self.lon_min)/self.lon_delta
        y=(lat-self.lat_min)/self.lat_delta
        return (int(x),int(y))
        
    def colour(self,lon: float,lat: float):
        (x,y)=self.to_xy(lon,lat)
        y=self.h-y
        if x<0 or y<0:
            return (0,0,0)
        if x>=self.w or y>=self.h:
            return (0,0,0)
        return self.img.getpixel((x,y))
    
    def create_df(self):
        data=[]
        lat_max=self.lat_min+self.lat_delta*(self.h-1)
        for i in range(self.w):
            for j in range(self.h):
                (r,g,b)=self.img.getpixel((i,j))
                color=r<<16|g<<8|b
                lon=self.lon_min+i*self.lon_delta
                lat=lat_max-j*self.lat_delta
                
                data.append([lon,lat,color,r,g,b])
                
        df=pd.DataFrame(data,columns=['lon','lat','color','r','g','b'])
        return df
        
    def add_to_df(self,df):
        colour=[]
        rc=[]
        gc=[]
        bc=[]
        
        for i,row in df.iterrows():
            (r,g,b)=self.colour(row['lon'],row['lat'])
            rc.append(r)
            gc.append(g)
            bc.append(b)
            colour.append(r<<16|g<<8|b)
        df['r']=rc
        df['g']=gc
        df['b']=bc
        df['color']=colour
        return df
    
# srtm maps
# tiles are referenced from their bottom left corner
class Srtm:
    def __init__(self,srtm_url: str = "https://dev-airlink.s3.amazonaws.com/srtm3/{y}{x}.hgt"):
        self.url=srtm_url
        self.data=None
        
    def get_tile(self, x: int,y: int):
        if x>=0:
            x="E{:03}".format(x)
        else:
            x="W{:03}".format(-x)
        if y>=0:
            y="N{:02}".format(y)
        else:
            y="S{:02}".format(-y)
        
        url= self.url.format(x=x,y=y)
        
        response=requests.get(url)
        size=len(response.content)
        dim=int(math.sqrt(size/2))
        if dim*dim*2!=size:
            print("Size error {} {} loading data from {}".format(size,dim*dim*2,full_path))
            return None
        
        data=np.frombuffer(response.content,np.dtype('>i2'),dim*dim).reshape(dim,dim)
        
        return data
        
    def get_data(self,x: float,y: float):
        return self.get_tile(math.floor(x),math.floor(y))

    def get_mosaic(self,lon_min: float,lat_min: float,lon_max: float,lat_max: float):
        x_min=math.floor(lon_min)
        x_max=math.floor(lon_max)+1
        y_min=math.floor(lat_min)
        y_max=math.floor(lat_max)+1
        
        tile=None
        for i in range(x_min,x_max):
            col=None
            for j in range(y_max-1,y_min-1,-1):
                tile=self.get_tile(i,j)
                if j==y_max-1:
                    col=tile
                else:
                    col=np.append(col,tile,axis=0)
            if i==x_min:
                self.data=col
            else:
                self.data=np.append(self.data,col,axis=1)
            
        self.data=np.flip(self.data,axis=0)
        (self.h,self.w)=self.data.shape

        (drows,dcols)=tile.shape
        self.lon_min=x_min
        self.lon_delta=1/dcols
        self.lat_min=y_min
        self.lat_delta=1/drows
        
        return (self.w,self.h)
    
    def altitude(self,lon: float,lat: float):
        x=int((lon-self.lon_min)/self.lon_delta)
        y=int((lat-self.lat_min)/self.lat_delta)
        return self.data[y,x]
    
    def create_df(self,downsize=10):
        data=[]
        for i in range(0,self.w,downsize):
            for j in range(0,self.h,downsize):
                lon=self.lon_min+i*self.lon_delta
                lat=self.lat_min+j*self.lat_delta
                altitude=self.data[j,i]
                if altitude<0:
                    altitude=0
                data.append([lon,lat,altitude])
        df=pd.DataFrame(data,columns=['lon','lat','amsl'])
        return df
    
    def add_to_df(self,df):
        column=[]
        for i,row in df.iterrows():
            column.append(self.altitude(row['lon'],row['lat']))
        df['amsl']=column
        return df

class NetCDF:
    def __init__(self):
        self.ds=None
        self.grp_name=""
        
    def load(self,file: str):
        self.ds=cdf.Dataset(file)
    
    def version(self):
        if self.ds is None:
            return None
        return self.ds.data_model
    
    def variables(self):
        for var in self.ds.variables:
            print("{} {} {}".format(var,self.ds.variables[var].long_name,self.ds.variables[var].size))
    
    def groups(self):
        if not hasattr(self.ds,'groups'):
            print('None')
        else:
            print(self.ds.groups)
            
    def set_group(self,name: str):
        self.grp_name=name
        # TODO 
    
    def describe(self):
        if self.ds is None:
            print('None')
        
        for element in self.ds.__dict__:
            print("{}: {}".format(element,self.ds.__dict__[element]))

    def set_lon(self,name: str):
        self.lon=self.ds.variables[name]
        self.lon_min=self.lon[0]
        self.lon_delta=abs(self.lon[1]-self.lon[0])
        
    def set_lat(self,name: str):
        self.lat=self.ds.variables[name]
        self.lat_min=self.lat[0]
        self.lat_delta=abs(self.lat[1]-self.lat[0])
        
    def limits(self):
        return (self.lon_min,self.lat_min,self.lon[-1],self.lat[-1])
    
    def set_values(self,name: str):
        self.val_name=name
        self.values=self.ds.variables[name]
        
    def value(self,lon,lat):
        x=int((lon-self.lon_min)/self.lon_delta)
        y=int((lat-self.lat_min)/self.lat_delta)
        return self.values[y,x]
        
    def create_df(self,downsize: int = 10):
        data=[]
        for i in range(0,len(self.lon),downsize):
            for j in range(0,len(self.lat),downsize):
                if type(self.values[j,i])==np.ma.core.MaskedConstant: # skip empty values
                    continue
                data.append([self.lon[i],self.lat[j],self.values[j,i]])
                
        df=pd.DataFrame(data,columns=['lon','lat',self.val_name])
        return df
    
    def create_missing_df(self,downsize: int = 10):
        data=[]
        for i in range(0,len(self.lon),downsize):
            for j in range(0,len(self.lat),downsize):
                if type(self.values[j,i])==np.ma.core.MaskedConstant: # keep empty values
                    data.append([self.lon[i],self.lat[j],0])
                
        df=pd.DataFrame(data,columns=['lon','lat',self.val_name])
        return df
                 
    def add_to_df(self,df):
        column=[]
        for i,row in df.iterrows():
            column.append(self.value(row['lon'],row['lat']))
        df[self.val_name]=column
        return df
        
# arcgis maps
gis=arcgis.GIS()
class Multispectral:
    def __init__(self,basemap='satellite'):
        #item=gis.content.search('"Multispectral Landsat"', 'Imagery Layer')[0] # TODO
        #item=gis.content.get('e5d1a8e30f414c8ba509c60deadfe529')
        item=gis.content.get('d9b466d6a9e647ce8d1dd5fe12eb434b') # prefered
        self.layer=item.layers[0] # TODO
        self.band_names=["unused","coastal_aerosol","blue","green","red","near_ir","swir1","swir2","cirrus","qa"]
        self.img_bands=None
    
    def reset(self):
        item=gis.content.get('d9b466d6a9e647ce8d1dd5fe12eb434b') # prefered
        self.layer=item.layers[0] # TODO
    
    def show_band_names(self):
        for i in range(len(self.band_names)):
            print("{} {}".format(i,self.band_names[i]))
            
    def enhance(self,image):
        return agf.stretch(image,
                       stretch_type='PercentClip',
                       min_percent=2, 
                       max_percent=2,
                       dra=True, 
                       gamma=[0.8,0.8,0.8])
    
    def get_image(self,lon_min: float,lat_min: float,lon_max: float,lat_max: float,band_ids=[4,3,2],enhance=False):
        if lat_max<lat_min:
            (lat_max,lat_min)=swap_values(lat_max,lat_min)
        if lon_max<lon_min:
            (lon_max,lon_min)=swap_values(lon_max,lon_min)
        
        bands=agf.extract_band(self.layer, band_ids)
        bands.extent={'xmax': '%.2f'%(lon_max)
                      , 'xmin': '%.2f'%(lon_min)
                      , 'ymax': '%.2f'%(lat_max)
                      , 'ymin': '%.2f'%(lat_min)
                      , 'spatialReference': 4326}
        print(bands.extent)
        if enhance:
            bands=self.enhance(bands)
        img_data=bands.export_image(f="json")
             
        print(img_data['href'])
        (self.lat_min,self.lon_min)=wm2gps.transform(img_data['extent']['xmin'],img_data['extent']['ymin'])
        (self.lat_max,self.lon_max)=wm2gps.transform(img_data['extent']['xmax'],img_data['extent']['ymax'])
        self.img_data=img_data
        self.rows=img_data['height']
        self.cols=img_data['width']
        
        response=requests.get(img_data['href'])
        self.img=Image.open(BytesIO(response.content))
        
        self.lon_delta=(self.lon_max-self.lon_min)/self.cols
        self.lat_delta=(self.lat_max-self.lat_min)/self.rows
        
        self.img_bands=[self.band_names[band_ids[0]],self.band_names[band_ids[1]],self.band_names[band_ids[2]]]
        
        return self.img
    
    def to_xy(self,lon: float,lat: float):
        x=(lon-self.lon_min)/self.lon_delta
        y=(lat-self.lat_min)/self.lat_delta
        return (int(x),int(y))
        
    def colour(self,lon: float,lat: float):
        (x,y)=self.to_xy(lon,lat)
        y=self.rows-y
        if x<0 or y<0:
            return [0,0,0]
        if x>=self.cols or y>=self.rows:
            return [0,0,0]
        return self.img.getpixel((x,y))
    
    def mercator_proportion(self):
        return mercator_proportion(self.lon_min,self.lat_min,self.lon_max,self.lat_max)
    
    def create_df(self):
        data=[]
        lat_max=self.lat_min+self.lat_delta*(self.rows-1)
        for i in range(self.cols):
            for j in range(self.rows):
                (r,g,b)=self.img.getpixel((i,j))
                lon=self.lon_min+i*self.lon_delta
                lat=lat_max-j*self.lat_delta
                
                data.append([lon,lat,r,g,b])
                
        df=pd.DataFrame(data,columns=['lon','lat',self.img_bands[0],self.img_bands[1],self.img_bands[2]])
        return df
        
    def add_to_df(self,df):
        colour=[]
        rc=[]
        gc=[]
        bc=[]
        
        for i,row in df.iterrows():
            [r,g,b]=self.colour(row['lon'],row['lat'])
            rc.append(r)
            gc.append(g)
            bc.append(b)
        df[self.img_bands[0]]=rc
        df[self.img_bands[1]]=gc
        df[self.img_bands[2]]=bc
        return df
                   
