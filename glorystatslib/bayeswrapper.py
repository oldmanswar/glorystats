# imports
import os
import time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# plotly library
import chart_studio.plotly as py
from plotly.offline import init_notebook_mode, iplot
init_notebook_mode(connected=True)
import plotly.graph_objs as go

# sklearn library
import sklearn
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_curve, auc, accuracy_score, precision_recall_fscore_support

from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer

# tensorflow library
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.utils import to_categorical 

import tensorflow as tf

# create an SVM explorer class
class SVMExplorer:
    # initialise the explorer with default parameters
    def __init__(self,name,kernel,C=1,gamma='scale',degree=3):
        self.name=name
        self.kernel=kernel
        self.C=C
        self.gamma=gamma
        self.degree=degree
        
        self.X_train= None 
        self.X_test= None 
        self.y_train= None 
        self.y_test= None 
        
        self.bayes_iter=5
        
        self.SVM=svm.SVC(kernel=kernel, C=C,gamma=gamma,degree=degree)
        
        # no, bs does not mean bull s**t, it's obvious
        self.bs=None
        
    def tweak_C(self,C):
        self.C=C
        self.SVM=svm.SVC(kernel=kernel, C=self.C,gamma=self.gamma,degree=self.degree)
    def tweak_gamma(self,gamma):
        self.gamma=gamma
        self.SVM=svm.SVC(kernel=kernel, C=self.C,gamma=self.gamma,degree=self.degree)
    def tweak_degree(self,degree):
        self.degree=degree
        self.SVM=svm.SVC(kernel=kernel, C=self.C,gamma=self.gamma,degree=self.degree)
        
    def set_data(self,X_train,y_train,X_test,y_test):
        self.X_train= X_train 
        self.X_test= X_test 
        self.y_train= y_train 
        self.y_test= y_test 
        
    def train(self):
        self.SVM.fit(self.X_train, self.y_train)
    
    def plot_margin(self):
        fig=plt.figure(figsize=(15,15))

        plt.subplot(2,2,1)
        plot_margin(self.X_train, self.y_train, self.SVM,"margin {} train".format(self.name))
        plt.subplot(2,2,2)
        plot_decisions(self.X_train, self.y_train, self.SVM,"decisions {} train".format(self.name))

        plt.subplot(2,2,3)
        plot_margin(self.X_test, self.y_test, self.SVM,"margin {} test".format(self.name))
        plt.subplot(2,2,4)
        plot_decisions(self.X_test, self.y_test, self.SVM,"decisions {} test".format(self.name))

        plt.show()
        
    def set_bayes_iter(self,iterations):
        self.bayes_iter=iterations
        
    # define a highly restricted bayes search function to avoid eternal wait times
    def bayes_search(self,c_min,c_max,gamma_min,gamma_max):
        self.bs=BayesSearchCV(
            svm.SVC(max_iter = 1000000,class_weight={0: 1, 1: 2.6735}), # got this limit from Phil Davitt, kudos to him
            {
                'C': Real(c_min,c_max,prior='log-uniform'),
                'gamma': Real(gamma_min, gamma_max, prior='log-uniform'),
                'degree': Categorical([self.degree]),#Integer(self.degree,self.degree+2),
                'kernel': Categorical([self.kernel]),
            },
            n_iter=self.bayes_iter,
            n_points=8,
            random_state=69 # it's the year in which I was born, stop thinking weird stuff
        )
        start_time=time.time()
        self.bs.fit(self.X_train,self.y_train.T[0], callback=lambda res: print("BayesSearchCV: params =", res.x_iters[-1]))
        end_time=time.time()
        print("Time taken: {} seconds".format(end_time-start_time))
        
    def bayes_train_score(self):
        print("Train score: {}".format(self.bs.score(self.X_train,self.y_train)))
              
    def bayes_test_score(self):
        print("Test score: {}".format(self.bs.score(self.X_test,self.y_test)))
        
    def bayes_print(self):
        print(self.bs.best_estimator_)
        
    
    def bayes_plot_margin(self):
        fig=plt.figure(figsize=(15,15))

        plt.subplot(2,2,1)
        plot_margin(self.X_train, self.y_train, self.bs.best_estimator_,"bayes margin {} train".format(self.name))
        plt.subplot(2,2,2)
        plot_decisions(self.X_train, self.y_train, self.bs.best_estimator_,"bayes decisions {} train".format(self.name))

        plt.subplot(2,2,3)
        plot_margin(self.X_test, self.y_test, self.bs.best_estimator_,"bayes margin {} test".format(self.name))
        plt.subplot(2,2,4)
        plot_decisions(self.X_test, self.y_test, self.bs.best_estimator_,"bayes decisions {} test".format(self.name))

        plt.show()

# create an RandomForest explorer class
class RandomForestExplorer:
    # initialise the explorer with default parameters
    def __init__(self,name,n_estimators=10,max_depth=20):
        self.name=name
        self.n_estimators=n_estimators
        self.max_depth=max_depth
        
        self.X_train= None 
        self.X_test= None 
        self.y_train= None 
        self.y_test= None 
        
        self.bayes_iter=5
        

        self.RFC=RandomForestClassifier(n_estimators=n_estimators,max_depth=max_depth)
        
        # no, bs does not mean bull s**t, it's obvious
        self.bs=None
        
    def tweak_n_estimators(self,n_estimators):
        self.n_estimators=n_estimators
        self.RFC=RandomForestClassifier(n_estimators=self.n_estimators,max_depth=self.max_depth)
    def tweak_max_depth(self,max_depth):
        self.max_depth=max_depth
        self.RFC=RandomForestClassifier(n_estimators=self.n_estimators,max_depth=self.max_depth)
       
    def set_data(self,X_train,y_train,X_test,y_test):
        self.X_train= X_train 
        self.X_test= X_test 
        self.y_train= y_train 
        self.y_test= y_test 
        
    def train(self):
        self.RFC.fit(self.X_train, self.y_train)
    
    def plot_margin(self):
        fig=plt.figure(figsize=(15,15))

        plt.subplot(2,2,1)
        plot_margin(self.X_train, self.y_train, self.RFC,"margin {} train".format(self.name))
        plt.subplot(2,2,2)
        plot_decisions(self.X_train, self.y_train, self.RFC,"decisions {} train".format(self.name))

        plt.subplot(2,2,3)
        plot_margin(self.X_test, self.y_test, self.RFC,"margin {} test".format(self.name))
        plt.subplot(2,2,4)
        plot_decisions(self.X_test, self.y_test, self.RFC,"decisions {} test".format(self.name))

        plt.show()
        
    def set_bayes_iter(self,iterations):
        self.bayes_iter=iterations
        
    # define a highly restricted bayes search function to avoid eternal wait times
    def bayes_search(self,ne_min,ne_max,md_min,md_max):
        self.bs=BayesSearchCV(
            RandomForestClassifier(),
            {
                'n_estimators': Integer(ne_min,ne_max),
                'max_depth': Integer(md_min, md_max),
            },
            n_iter=self.bayes_iter,
            n_points=8,
            random_state=69 # it's the year in which I was born, stop thinking weird stuff
        )
        start_time=time.time()
        self.bs.fit(self.X_train,self.y_train.T[0], callback=lambda res: print("RandomForest: params =", res.x_iters[-1]))
        end_time=time.time()
        print("Time taken: {} seconds".format(end_time-start_time))
        
    def bayes_train_score(self):
        print("Train score: {}".format(self.bs.score(self.X_train,self.y_train)))
              
    def bayes_test_score(self):
        print("Test score: {}".format(self.bs.score(self.X_test,self.y_test)))
        
    def bayes_print(self):
        print(self.bs.best_estimator_)
        
    
    def bayes_plot_margin(self):
        fig=plt.figure(figsize=(15,15))

        plt.subplot(2,2,1)
        self.plot_margin(self.X_train, self.y_train, self.bs.best_estimator_,"bayes margin {} train".format(self.name))
        plt.subplot(2,2,2)
        plot_decisions(self.X_train, self.y_train, self.bs.best_estimator_,"bayes decisions {} train".format(self.name))

        plt.subplot(2,2,3)
        self.plot_margin(self.X_test, self.y_test, self.bs.best_estimator_,"bayes margin {} test".format(self.name))
        plt.subplot(2,2,4)
        plot_decisions(self.X_test, self.y_test, self.bs.best_estimator_,"bayes decisions {} test".format(self.name))

        plt.show()
