# Using NASA's Shuttle Radar Topography Mission data to add context to a local LIDAR survey
The purpose of this notebook is to show how to combine geographical data from two different sources and in two different formats. A method for displaying the data in three dimensions is also shown including HTML exports for showing it online.

## LiDAR survey data
The LiDAR survey data comes from the INFOMAR programme participated by the [Marine Institute](http://www.marine.ie/) and [Geological Survey Ireland](https://www.gsi.ie/). It can be obtained through [SeaDataNet](https://www.seadatanet.org/).

## Shuttle Radar Topography Mission data
The Shuttle Radar Topography Mission data (SRTM) comes from NASA and can be obtained through the Earth Resources Observation and Science [EROS](https://www.usgs.gov/centers/eros) center.

## Process
The process is relatively simple:
### Data is downloaded in their respective format
### Data is converted into NumPy arrays where lines and columns are assigned equivalent geographical coordinates
### Blank data points in the survey data are filled with interpolation points from the SRTM data

_Survey data_ ([3D Model](https://oldmanswar.gitlab.io/glorystats/survey.html))
![Survey data](survey.png "AnVIL Portal Image!")
_SRTM data_ ([3D Model](https://oldmanswar.gitlab.io/glorystats/srtm.html))
![SRTM data](srtm.png "AnVIL Portal Image!")
_Combined data_ ([3D Model](https://oldmanswar.gitlab.io/glorystats/combined.html))
![Combined data](combined.png "AnVIL Portal Image!")
